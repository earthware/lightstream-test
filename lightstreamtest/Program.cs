﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using lightstreamtest.Models;
using lightstreamtest.Models.Requests;
using Newtonsoft.Json;

namespace lightstreamtest
{
    class Program
    {
        private static readonly string baseUrl = "https://cloud.golightstream.com/api/v1/";
        private static readonly string apiKey = "wea6vAbHAZN6JA2VUnXrNU1wGSS56CBA";

        static void Main(string[] args)
        {
            var newProject = CreateProject();

            var streamKey = CreateStreamKey(newProject.Id);

            Console.WriteLine($"{streamKey.Ingest}{streamKey.Key}");

            var previewLayout = CreateLayout(newProject.Id);
            var liveLayout = CreateLayout(newProject.Id);

            var previewLayoutConsumer = CreateLayoutConsumer(previewLayout.Id, "rtmp://ingest.golightstream.com/inbound");
            var liveLayoutConsumer = CreateLayoutConsumer(liveLayout.Id, "rtmp://ingest.golightstream.com/inbound");

            Console.WriteLine($"Preview - {previewLayoutConsumer.Ingest}/{previewLayoutConsumer.Key}");
            Console.WriteLine($"Live - {liveLayoutConsumer.Ingest}/{liveLayoutConsumer.Key}");

            // Preview assets
            var layoutAsset = CreateLayoutAsset(previewLayout.Id, new AssetRequest 
            { 
                Type = "media",
                Url = $"{streamKey.Ingest}{streamKey.Key}",
                Height = 720,
                Width = 1280,
                Order = 1
            });

            var overlayAsset = CreateLayoutAsset(previewLayout.Id, new AssetRequest
            {
                Type = "overlay",
                Url = "https://google.com",
                Width = 400,
                Height = 400,
                Order = 0,
                Left = 1280 - 400
            });

            // live assets
            var liveLayoutAsset = CreateLayoutAsset(liveLayout.Id, new AssetRequest
            {
                Type = "media",
                Url = $"{streamKey.Ingest}{streamKey.Key}",
                Height = 720,
                Width = 1280,
                Order = 1
            });

            var liveLayoutNew = CreateLayout(newProject.Id);
            var liveLayoutNewConsumer = CreateLayoutConsumer(liveLayoutNew.Id, "rtmp://ingest.golightstream.com/inbound");
            Console.WriteLine($"LiveNew - {liveLayoutNewConsumer.Ingest}/{liveLayoutNewConsumer.Key}");

            CreateLayoutAsset(liveLayoutNew.Id, new AssetRequest
            {
                Type = "media",
                Url = $"{streamKey.Ingest}{streamKey.Key}",
                Height = 720,
                Width = 1280,
                Order = 1
            });

            CreateLayoutAsset(liveLayoutNew.Id, new AssetRequest
            {
                Type = "overlay",
                Url = "https://google.com",
                Width = 400,
                Height = 400,
                Order = 0,
                Left = 1280 - 400
            });

            CreateLayoutAsset(liveLayoutNew.Id, new AssetRequest
            {
                Type = "overlay",
                Url = "https://google.com",
                Width = 400,
                Height = 400,
                Order = 0
            });

            // In order to add to a layout so that changes appear, it seems you need to publish the layout
            var start = StartBroadcast(liveLayoutNew.Id);

            var stop = StopBroadcast(liveLayoutNew.Id);
            var deletedProjectJson = DeleteProject(newProject.Id);
            var deletedConsumerJson = DeleteLayoutConsumer(liveLayoutNew.Id);
            var deletedLayoutJson = DeleteLayout(liveLayoutNew.Id);

            
        }

        private static Project CreateProject()
        {
            var endpoint = "project";
            var url = $"{baseUrl}{endpoint}";
            var body = new Meta { Name = "Demo Project Earthware" };

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("apiKey", apiKey);

                var content = JsonConvert.SerializeObject(body);
                var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                var byteContent = new ByteArrayContent(buffer);

                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                var response = client.PostAsync(url, byteContent).Result;
                var json = response.Content.ReadAsStringAsync().Result;

                return JsonConvert.DeserializeObject<Project>(json);
            }
        }

        private static StreamKey CreateStreamKey(string projectId)
        {
            var endpoint = $"project/{projectId}/streamkey";
            var url = $"{baseUrl}{endpoint}";
            
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("apiKey", apiKey);

                var response = client.PostAsync(url, null).Result;
                var json = response.Content.ReadAsStringAsync().Result;

                return JsonConvert.DeserializeObject<StreamKey>(json);
            }
        }

        private static Layout CreateLayout(string projectId)
        {
            var endpoint = $"project/{projectId}/layout";
            var url = $"{baseUrl}{endpoint}";

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("apiKey", apiKey);

                var response = client.PostAsync(url, null).Result;
                var json = response.Content.ReadAsStringAsync().Result;

                return JsonConvert.DeserializeObject<Layout>(json);
            }
        }

        private static Consumer CreateLayoutConsumer(string layoutId, string ingest, string type = "rtmp")
        {
            var endpoint = $"layout/{layoutId}/consumer";
            var url = $"{baseUrl}{endpoint}";

            var body = new ConsumerRequest { Key = Guid.NewGuid().ToString(), Ingest = ingest, Type = type };

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("apiKey", apiKey);

                var content = JsonConvert.SerializeObject(body);
                var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                var byteContent = new ByteArrayContent(buffer);

                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                var response = client.PostAsync(url, byteContent).Result;
                var json = response.Content.ReadAsStringAsync().Result;

                return JsonConvert.DeserializeObject<Consumer>(json);
            }
        }

        private static MediaAsset CreateLayoutAsset(string layoutId, AssetRequest assetRequest)
        {
            var endpoint = $"layout/{layoutId}/asset";
            var url = $"{baseUrl}{endpoint}";

            var body = assetRequest;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("apiKey", apiKey);

                var content = JsonConvert.SerializeObject(body);
                var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                var byteContent = new ByteArrayContent(buffer);

                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                var response = client.PostAsync(url, byteContent).Result;
                var json = response.Content.ReadAsStringAsync().Result;

                return JsonConvert.DeserializeObject<MediaAsset>(json);
            }
        }

        private static string GetLayout(string layoutId)
        {
            var endpoint = $"layout/{layoutId}";
            var url = $"{baseUrl}{endpoint}";

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("apiKey", apiKey);
                var response = client.GetAsync(url).Result;
                var json = response.Content.ReadAsStringAsync().Result;

                return json;
            }
        }

        private static string StartBroadcast(string layoutId)
        {
            var endpoint = $"layout/{layoutId}/publish";
            var url = $"{baseUrl}{endpoint}";

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("apiKey", apiKey);

                var response = client.PostAsync(url, null).Result;
                var json = response.Content.ReadAsStringAsync().Result;

                return json;
            }
        }

        private static string StopBroadcast(string layoutId)
        {
            var endpoint = $"layout/{layoutId}/unpublish";
            var url = $"{baseUrl}{endpoint}";

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("apiKey", apiKey);

                var response = client.PostAsync(url, null).Result;
                var json = response.Content.ReadAsStringAsync().Result;

                return json;
            }
        }

        private static string GetLayoutAssets(string layoutId)
        {
            var endpoint = $"layout/{layoutId}/asset";
            var url = $"{baseUrl}{endpoint}";

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("apiKey", apiKey);

                var response = client.GetAsync(url).Result;
                var json = response.Content.ReadAsStringAsync().Result;

                return json;
            }
        }

        private static string UpdateLayout(string layoutId)
        {
            var endpoint = $"layout/{layoutId}";
            var url = $"{baseUrl}{endpoint}";

            var body = new UpdateLayoutRequest()
            {
                Id = layoutId,
                Height = 720,
                Width = 1280,
                Bitrate = 2900,
                Framerate = 30
            };

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("apiKey", apiKey);

                var content = JsonConvert.SerializeObject(body);
                var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                var byteContent = new ByteArrayContent(buffer);

                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                var response = client.PatchAsync(url, byteContent).Result;
                var json = response.Content.ReadAsStringAsync().Result;

                return json;
            }
        }

        private static string DeleteLayoutConsumer(string id)
        {
            var endpoint = $"consumer/{id}";
            var url = $"{baseUrl}{endpoint}";

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("apiKey", apiKey);

                var response = client.DeleteAsync(url).Result;
                var json = response.Content.ReadAsStringAsync().Result;

                return json;
            }
        }

        private static string DeleteLayout(string id)
        {
            var endpoint = $"layout/{id}";
            var url = $"{baseUrl}{endpoint}";

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("apiKey", apiKey);

                var response = client.DeleteAsync(url).Result;
                var json = response.Content.ReadAsStringAsync().Result;

                return json;
            }
        }

        private static string DeleteProject(string id)
        {
            var endpoint = $"project/{id}";
            var url = $"{baseUrl}{endpoint}";

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("apiKey", apiKey);

                var response = client.DeleteAsync(url).Result;
                var json = response.Content.ReadAsStringAsync().Result;

                return json;
            }
        }
    }
}
