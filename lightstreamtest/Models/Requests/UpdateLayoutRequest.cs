﻿using Newtonsoft.Json;

namespace lightstreamtest.Models.Requests
{
    public class UpdateLayoutRequest
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("height")]
        public int Height { get; set; }

        [JsonProperty("width")]
        public int Width { get; set; }

        [JsonProperty("bitrate")]
        public int Bitrate { get; set; }

        [JsonProperty("framerate")]
        public int Framerate { get; set; }
    }
}
