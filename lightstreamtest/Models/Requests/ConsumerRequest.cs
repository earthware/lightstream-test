﻿using Newtonsoft.Json;

namespace lightstreamtest.Models.Requests
{
    public class ConsumerRequest
    {
        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("ingest")]
        public string Ingest { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }
}
