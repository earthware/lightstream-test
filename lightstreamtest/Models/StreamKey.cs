﻿namespace lightstreamtest.Models
{
    public class StreamKey
    {
        public string Key { get; set; }

        public string Ingest { get; set; }

        public string Id { get; set; }

        public bool Enabled { get; set; }
    }
}
