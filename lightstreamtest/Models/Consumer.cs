﻿namespace lightstreamtest.Models
{
    public class Consumer
    {
        public string Id { get; set; }

        public string Key { get; set; }

        public string Ingest { get; set; }

        // convert to enum
        public string Type { get; set; }

        public bool Enabled { get; set; }
    }
}
