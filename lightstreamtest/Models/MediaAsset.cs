﻿namespace lightstreamtest.Models
{
    public class MediaAsset
    {
        public string Type { get; set; }

        public string Url { get; set; }

        public int Top { get; set; }

        public int Left { get; set; }

        public int Height { get; set; }

        public int Width { get; set; }

        public int Opacity { get; set; }

        public int Order { get; set; }

        public int Volume { get; set; }
    }
}
