﻿namespace lightstreamtest.Models
{
    public class Layout
    {
        public string Id { get; set; }

        public int Height { get; set; }

        public int Width { get; set; }

        public int Bitrate { get; set; }

        public int FrameRate { get; set; }
    }
}
