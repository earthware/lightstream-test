﻿using System;
using Newtonsoft.Json;

namespace lightstreamtest.Models
{
    public class Project
    {
        public string Id { get; set; }

        public Meta Meta { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }

    public class Meta
    {
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
